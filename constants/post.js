const { TwitterApi } = require("twitter-api-v2");

const {
  TWITTER_CONSUMER,
  TWITTER_CONSUMER_SECRET,
  TWITTER_ACCESS_TOKEN,
  TWITTER_ACCESS_TOKEN_SECRET,
  TWITTER_BEARER,
  TWITTER_APP_ID,
} = require("../../../env");

/*//console.log({
  appKey: TWITTER_CONSUMER,
  appSecret: TWITTER_CONSUMER_SECRET,
  accessToken: TWITTER_ACCESS_TOKEN,
  accessSecret: TWITTER_ACCESS_TOKEN_SECRET,
});*/
const client = new TwitterApi({
  appKey: TWITTER_CONSUMER,
  appSecret: TWITTER_CONSUMER_SECRET,
  accessToken: TWITTER_ACCESS_TOKEN,
  accessSecret: TWITTER_ACCESS_TOKEN_SECRET,
});
const bearer = new TwitterApi(TWITTER_BEARER);

const twitterClient = client.readWrite;
//const twitterBearer = bearer.readOnly;

module.exports.tweet = async (msg, img) => {
  try {
    if (img === "") {
      await twitterClient.v2.tweet({
        text: msg,
      });
    } else {
      const mediaId = await twitterClient.v1.uploadMedia(img);
      await twitterClient.v2.tweet({
        text: msg,
        media: {
          media_ids: [mediaId],
        },
      });
    }
  } catch (e) {
    //console.log(e);
  }
};
