const { tweet } = require("../constants/post");

module.exports = async (_, { msg, media, twitter }, { currentUser }) => {
  ////console.log(check)
  if (!currentUser) {
    throw new Error("Not authenicated!");
  }
  try {
    if (twitter && twitter === true) {
      tweet(msg, "");
    }
    return true;
  } catch (err) {
    console.log(err);
  }
};
