const sendPost = require("./resolvers/send_post.js");

module.exports = {
  Query: {
    sendPost,
  },
};
